import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import * as bcrypt from 'bcrypt';
import { Task } from "src/tasks/task.entity";



@Entity() 
@Unique(['username']) //we are just define that the "username" column in the "user" table should be usinque (this happen on the databse level)
export class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number ;

    @Column()
    username: string ; 

    @Column()
    password: string ; 

    @Column()
    salt: string;


    @OneToMany(type => Task, task => task.user, { eager: true }) // the eager side is the user : whenever we retrieve the user as an object we can access "user.tasks" immiadiatly and we get an array of tasks owend by the same users .
    tasks: Task[];





    async ValidatePassword (password: string): Promise<boolean>{
        const hash = await bcrypt.hash(password, this.salt);
        return hash === this.password;
    }
}