import { Body, Controller, Post, ValidationPipe, UsePipes, UnauthorizedException, UseGuards, Req } from '@nestjs/common'; 
import { AuthGuard } from '@nestjs/passport';
import { getCustomRepository } from 'typeorm';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from "./dto/auth-credentails-dto"
import { User } from './user.entity'; 
import { GetUser } from './get-user.decorator'


@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService){} 

    @Post('/signup') 
    signUp(@Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto) {
        //console.log(authCredentialsDto)
        return this.authService.signUp(authCredentialsDto)
    }

    @Post('/signin')
    signin(@Body() authCredentialsDto: AuthCredentialsDto ): Promise<{ accessToken: string }> {
        return this.authService.signIn(authCredentialsDto); 
        
    }

    @Post('/test') // it's only for test purpose
    @UseGuards(AuthGuard())
    test1(@Req() req) {  // @Req() to retrieve all the request, not only the Body or Param.
        console.log(req.user);
    }
}
