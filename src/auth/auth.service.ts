import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository'
import { AuthCredentialsDto } from './dto/auth-credentails-dto'
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';


@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(UserRepository)    //when the service is initialized , we are going to inject UserRepository instance into userRepository.
        private userRepository: UserRepository,
        private jwtservice: JwtService) {}

    async signUp(authCredentialsDto: AuthCredentialsDto) {
        return this.userRepository.signUp(authCredentialsDto)
    }    

    async signIn(authCredentialsDto: AuthCredentialsDto): Promise<{ accessToken: string }> {
        const username = await this.userRepository.ValidateUserPassword(authCredentialsDto) 
        if (!username) {
            throw new UnauthorizedException('Invalid Credentials')
        }

        const payload: JwtPayload = {username}; 
        const accessToken = await this.jwtservice.sign(payload); // generate the token 

        return { accessToken } // return an object that contain onlythe token 

    }
}
