import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt-strategy';
import * as config from 'config'


const jwtConfig = config.get('jwt')

@Module({
  imports: [
    PassportModule.register({defaultStrategy:'jwt' }), //this module imported, export a service (provided) called "JwtService" so we can inject it using dependency injection
    JwtModule.register({
      secret: process.env.JWT_SECRET || jwtConfig.secret,
      signOptions: {
        expiresIn: jwtConfig.expiresIn,
      }
    }),
    TypeOrmModule.forFeature([UserRepository])], ///here we can provide Entity or Repository .
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy], 
  exports: [JwtStrategy, PassportModule] //make it possible to guard other operation with jwt and passport-js
})
export class AuthModule {}
