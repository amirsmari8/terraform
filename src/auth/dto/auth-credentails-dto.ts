import { IsNotEmpty, IsString, Matches, MaxLength, MinLength } from "class-validator";

export class AuthCredentialsDto {
    @MinLength(4)
    @MaxLength(20)
    @IsString()
    @IsNotEmpty()
    username: string; 
    
    @MinLength(8)
    @IsString()
    @IsNotEmpty()
    // @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message: "password is too weak"})
    password: string;
}