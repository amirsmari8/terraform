import { EntityRepository, Repository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialsDto } from './dto/auth-credentails-dto'
import { ConflictException, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import * as bcrypt from 'bcrypt'

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async signUp(authCredentialsDto: AuthCredentialsDto) {
        const { username, password } = authCredentialsDto;

        const salt = await bcrypt.genSalt(); 
        //console.log(salt)
        
        // const exist = this.findOne({username}) // here we need to send to query to the database (find.one and save) 
        // if (exist) {                           // the best practise is to use just one query
        //     throw new NotFoundException({message : "user is found or duplicated"})  // see the unique decorator at user.entity.ts
        // }

        const user = new User(); 
        user.username = username; 
        user.password = await this.hashPassword(password,salt)
        user.salt = salt
        try {await user.save() 
        } catch (error) {
            if (error.code === '23505') {  // you can console.log(error.code) , it's of type string
                throw new ConflictException ('username already exist') //// see the unique decorator at user.entity.ts
            }
            else {
                throw new InternalServerErrorException();
            }
        }
        
        //return user ; dont return the user because it contains the password .  
    }

    async ValidateUserPassword (authCredentialsDto: AuthCredentialsDto) {
        const { username, password } = authCredentialsDto; 
        const user = await this.findOne({username})

        if (user && await user.ValidatePassword(password)) { // return True or False
            return user.username
        }
        else {
            return null;
        }

    }

    private async hashPassword(password:string, salt:string): Promise<string> {
        return bcrypt.hash(password,salt);
    }
}