import { createParamDecorator, Post, UseGuards } from "@nestjs/common"; 
import { User } from "./user.entity";

export const GetUser = createParamDecorator((data, req): User => {
    //console.log(req) // in this console.log you will see a section containe user .
    return req.user;
})



// // to test this : 
// you can add a route in the controller : 

// @Post('/test') 
// @UseGuards(AuthGuard)     //import { AuthGuard } from "@nestjs/passport";
// test(@GetUser() user: User){
//     console.log(user)
// }

// or 

// @Post('/test') 
// @UseGuards(AuthGuard)     //import { AuthGuard } from "@nestjs/passport";
// test(@Req() req){         // @Req to retreive the entire request
//     console.log(req)     // you will see the field in the log "user"
// }
