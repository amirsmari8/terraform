import { EntityRepository, Repository } from "typeorm";
import { Task } from "./task.entity";
import { CreateTaskDto } from './dto/create-task-dto'
import { TaskStatus } from './task.status.enum'
import { GetTaskFilterDto } from './dto/get-tasks-filer-dto'
import { User } from "src/auth/user.entity";
import { InternalServerErrorException, Logger } from "@nestjs/common";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>{ 
    private logger = new Logger('TaskRepository');

    async createTask(createTaskDto: CreateTaskDto, req): Promise<Task>{
        const { title, descreption } = createTaskDto; 
        const task = new Task ()
        task.title = title; 
        task.descreption = descreption; 
        task.status = TaskStatus.OPEN; 
        
        const users : User= req.user // we can say also task.user = req.user
        task.user = users 

  
        await task.save(); 

        delete task.user // when we return the task , this task contain sensative information like password , for that we need to delete it .

        return task;
    }

    async getTasks(filterDTO: GetTaskFilterDto, req) {
        const { status, search } = filterDTO; 
        const query = this.createQueryBuilder('task');   //method of Repository that interact with task table

        query.where('task.userId = :userId', { userId : req.user.id }) // get all task by userId , "userId" is a column in table created by typeOrm but we need to define it i the entity

        if(status){
            query.andWhere('task.status = :status', { status });
        }

        if(search){
            query.andWhere('(task.title LIKE :search OR task.descreption LIKE :search)', {search: `%${search}%`});
        }


        try {
        const tasks = await query.getMany();
        return tasks;
        } catch(error) {
            this.logger.error(`filed to get task for user ${req.user.username}, DTO is ${JSON.stringify(filterDTO)}`)
            throw new InternalServerErrorException();
        }
        
    }

}