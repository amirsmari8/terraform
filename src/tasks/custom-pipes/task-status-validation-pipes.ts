// extends means:
// The new class is a child. It gets benefits coming with inheritance. It has all properties, methods as its parent. It can override some of these and implement new, but the parent stuff is already included.

// implements means:
// The new class can be treated as the same "shape", while it is not a child. It could be passed to any method where the Person is required, regardless of having different parent than Person


import { BadRequestException, PipeTransform } from "@nestjs/common";
import { TaskStatus } from "../task.status.enum";


export class TaskStatusValidationPipes implements PipeTransform {
    
    readonly allowedStatuses = [
        TaskStatus.OPEN, 
        TaskStatus.IN_PROGRESS, 
        TaskStatus.DONE,
    ]
    
    transform(value: any) {
        value = value.toUpperCase();
        if (!this.isStatusValid(value)) {
            throw new BadRequestException (`${value} is an invalid status`)
        }
        return value;
    }

    private isStatusValid(status: any) {
        const idx = this.allowedStatuses.indexOf(status); 
        return idx !== -1; 
    }
}