import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { title } from 'process';
import { Not } from 'typeorm';
import { CreateTaskDto } from './dto/create-task-dto';
import { Task } from './task.entity';
import { TaskRepository } from './task.repository'
import { TaskStatus } from './task.status.enum';
import { GetTaskFilterDto } from './dto/get-tasks-filer-dto'




@Injectable()
export class TasksService {
    constructor(
       @InjectRepository(TaskRepository)
       private taskRepository: TaskRepository
    ){}



    async getTaskById(id: number, req): Promise<Task> {

      const found = await this.taskRepository.findOne({ where : {id, userId: req.user.id} })
      if (!found) {
         throw new NotFoundException(`this task with id ${id} is not found`)
      }
      return found;
    }


    async createTask(createTaskDto: CreateTaskDto, req): Promise<Task>{
          return this.taskRepository.createTask(createTaskDto, req )
    }

    async deleteTask(id: number, req){
      // const found = await this.getTaskById(id); 
      // if (!found){
      //    throw new NotFoundException(`this task with id ${id} is not found`)
      // }
      // await this.taskRepository.delete(id)
      const result = await this.taskRepository.delete({id, userId: req.user.id}) 
      console.log(result); 
      if (result.affected ===  0) {
         throw new NotFoundException (`this task with id ${id} is not found`)
      }
    }


    async updateTaskStatus(id: number, status: TaskStatus, req): Promise<Task>{
      const found = await this.getTaskById(id, req.user)
      if (!found){
         throw new NotFoundException (`the task with id ${id} is not found .`)
      }
      found.status = status 
      await found.save()
      return found
      
    }

    getTasks(filterDTO: GetTaskFilterDto, req) {
      return this.taskRepository.getTasks(filterDTO, req)
    }

   //   //tasks: Task[] = []; 

   //   getAllTasks(): Task[] {
   //       return this.tasks;
   //   }

   //   getTaskWithFilters(filterDTO){
   //      const { status, search } = filterDTO;
   //      let tasks = this.getAllTasks()
        
   //      if (status) {
   //         tasks = tasks.filter(task => task.status === status)
   //      }
   //      if (search) {
   //         tasks = tasks.filter(task => 
   //          task.title.includes(search) ||
   //          task.descreption.includes(search) )
   //      }
   //      return tasks
   //   }


   //   createTask(createTaskDto: CreateTaskDto): Task  {   
   //      const { title, descreption } = createTaskDto   //distruct the dto object
   //      const task: Task = {
   //          id: uuidv1(),
   //          title, 
   //          descreption, 
   //          status: TaskStatus.OPEN
   //      }
   //      this.tasks.push(task); 
   //      return task;
   //   }

   //   deleteTask(id: string) { 
   //     const found = this.getTaskById(id) 
   //     return this.tasks.filter( task => task.id !== found.id )            
   //   }

   //   updateTaskStatus(id: string, status: TaskStatus): Task{
   //      const task = this.getTaskById(id); 
   //      task.status = status;
   //      return task;
   //   }
     

}
