import { Body, Controller, Delete, Get, Logger, NotFoundException, Param, ParseIntPipe, Patch, Post, Query, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { TasksService } from './tasks.service'; 
import { CreateTaskDto } from './dto/create-task-dto'
import { TaskStatusValidationPipes } from './custom-pipes/task-status-validation-pipes'
import { Task } from './task.entity';
import { TaskStatus } from './task.status.enum'
import { GetTaskFilterDto } from './dto/get-tasks-filer-dto'
import { AuthGuard } from '@nestjs/passport';



@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
    private logger = new Logger('TaskController');
    constructor(private tasksService: TasksService){}
    
    // @Get()
    // getTasks(@Query() filterDTO): Task[] {    // la lagique filterDTO:GetTaskFilterDto
    //     //console.log(filterDTO); 
    //     if (Object.keys(filterDTO).length) {
    //         return this.tasksService.getTaskWithFilters(filterDTO);
    //     }else {
    //         return this.tasksService.getAllTasks()
    //     }
    // }
    @Get() 
    getTasks(@Query(ValidationPipe) filterDTO: GetTaskFilterDto, @Req() req) {
        this.logger.verbose(`user is ${req.user.username} retrieving all task. the filter is ${JSON.stringify(filterDTO)}`)
        return this.tasksService.getTasks(filterDTO, req)
    }

    @Post()
    @UsePipes(ValidationPipe)
    createTask(@Body() createTaskDto: CreateTaskDto, @Req() req ): Promise<Task>{
        //console.log(createTaskDto)
        this.logger.verbose(`the user ${req.user.username} is creating a task. Data is ${JSON.stringify(createTaskDto)}`)
        return this.tasksService.createTask(createTaskDto, req);
    }

    @Get('/:id')
    getTask(@Param('id', ParseIntPipe) id: number, @Req() req): Promise<Task> {
        return this.tasksService.getTaskById(id, req);
    }

    @Delete('/:id') 
    deleteTask(@Param('id', ParseIntPipe) id: number, @Req() req) {
        return this.tasksService.deleteTask(id, req);
    }

    @Patch('/:id/status') 
    updateTask(@Param('id', ParseIntPipe) id: number, @Body('status', TaskStatusValidationPipes) status: TaskStatus, @Req() req){
        return this.tasksService.updateTaskStatus(id, status, req)
    }
}
